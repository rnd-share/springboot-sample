package com.example.demo.service;

import com.example.demo.dto.Karyawan;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class KaryawanService {

	public List<Karyawan> getAll(){
		List<Karyawan> karyawanList = new ArrayList<>();
//		CARA 1
		Karyawan karyawan = new Karyawan();
		karyawan.setNip("xxx");
//		CARA 2
		Karyawan karyawan2 = new Karyawan("xx1","adam");
//		CARA 3
		Karyawan karyawan3 = Karyawan.builder()
				.nama("alexander")
				.nip("xx2")
				.build();
		karyawanList.add(karyawan);
		karyawanList.add(karyawan2);
		karyawanList.add(karyawan3);
		return karyawanList;
	}
}
