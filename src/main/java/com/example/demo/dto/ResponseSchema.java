package com.example.demo.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ResponseSchema<T> {
	private String errorCode;
	private String errorMessage;
	private T data;
}
