package com.example.demo.controller;

import com.example.demo.dto.Karyawan;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping
public class HelloController {


	@GetMapping("hello")
	public List<Karyawan> hello() {
		List<Karyawan> karyawanList = new ArrayList<>();
//		CARA 1
		Karyawan karyawan = new Karyawan();
		karyawan.setNip("xxx");
		karyawan.setNama("fandy");
//		CARA 2
		Karyawan karyawan2 = new Karyawan("xx1","adam");
//		CARA 3
		Karyawan karyawan3 = Karyawan.builder()
				.nama("alexander")
				.nip("xx2")
				.build();
		karyawanList.add(karyawan);
		karyawanList.add(karyawan2);
		karyawanList.add(karyawan3);
		return karyawanList;
	}


}
