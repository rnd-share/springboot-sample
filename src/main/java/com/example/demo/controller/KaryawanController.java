package com.example.demo.controller;

import com.example.demo.dto.Karyawan;
import com.example.demo.dto.ResponseSchema;
import com.example.demo.service.KaryawanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/karyawan")
public class KaryawanController {

	@Autowired
	KaryawanService karyawanService;

	@GetMapping
	public ResponseEntity<ResponseSchema> getKaryawan(){
		return ResponseEntity
				.ok()
				.body(
						ResponseSchema.builder()
								.errorCode("EMP-00-000")
								.errorMessage("success")
								.data(karyawanService.getAll())
								.build()
				);
	}

	@PostMapping("/insert")
	public ResponseEntity<ResponseSchema> addKaryawan(@RequestBody Karyawan karyawan) {
		return ResponseEntity
				.ok()
				.body(
						ResponseSchema.builder()
								.errorCode("EMP-00-000")
								.errorMessage("success")
								.data(karyawan)
								.build()
				);
	}


}
